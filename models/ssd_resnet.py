from math import sqrt
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
from torch.autograd import Variable
from layers.modules.l2norm import L2Norm
from torchvision import transforms
import itertools
import os
import numpy as np
from utils import *

# from torchvideotransforms import video_transforms, volume_transforms


device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

class AuxiliaryConvolutions(nn.Module):
    """ Auxiliary Convolottion following the structure of SSD auxiliary convolutions
    """
    def __init__(self, n_classes):
        super(AuxiliaryConvolutions, self).__init__()

        # Auxiliary convolutions added to base network
        self.conv1_1 = nn.Conv3d(512, 256, kernel_size=(1, 1, 1), padding=0)
        self.conv1_2 = nn.Conv3d(256, 512, kernel_size=(1, 3, 3), stride=2, padding=1)
        
        self.conv2_1 = nn.Conv3d(512, 128, kernel_size=(1, 1, 1), padding=0)
        self.conv2_2 = nn.Conv3d(128, 256, kernel_size=(1, 3, 3), stride=2, padding=1)

        self.conv3_1 = nn.Conv3d(256, 128, kernel_size=(1, 1, 1), padding=0)
        self.conv3_2 = nn.Conv3d(128, 256, kernel_size=(1, 3, 3), padding=0)  

        self.conv4_1 = nn.Conv3d(256, 128, kernel_size=(1, 1, 1), padding=0)
        self.conv4_2 = nn.Conv3d(128, 256, kernel_size=(1, 3, 3), padding=0) 

        self.pool4 = nn.AdaptiveAvgPool3d(n_classes * 10)
        self.fc1 = nn.Linear(512, n_classes)
        # self.fc2 = nn.linear

    def forward(self, conv_resnet3d_feats):
        x = F.relu(self.conv1_1(conv_resnet3d_feats))
        x = F.relu(self.conv1_2(x))
        conv_aux_1_feats = x

        x = F.relu(self.conv2_1(x))
        x = F.relu(self.conv2_2(x))
        conv_aux_2_feats = x

        x = F.relu(self.conv3_1(x))
        x = F.relu(self.conv3_2(x))
        conv_aux_3_feats = x

        x = F.relu(self.conv4_1(x))
        x = F.relu(self.conv4_2(x))
        conv_aux_4_feats = x

        # Perform classification results
        x = x.squeeze(4).squeeze(3)
        x = x.view(-1, 512)
        final_labels = F.softmax(self.fc1(x), dim=1)

        return conv_aux_1_feats, conv_aux_2_feats, conv_aux_3_feats, conv_aux_4_feats, final_labels


class PredictionConvolution(nn.Module):
    """
    Convolutions used to map location prediction to feature space
    """
    def __init__(self, n_clip_bboxes, n_classes):
        """
        :param n_clip_bboxes: number of bounding boxes per clip
        :param n_classes: number of actions
        """
        super(PredictionConvolution, self).__init__()

        self.n_classes = n_classes
        self.n_clip_bboxes = n_clip_bboxes

        # Define number of priors per grid location in feature map
        n_boxes = {
            'conv_resnet_3': 4,
            "conv_resnet_4": 6,
            "conv_aux_1": 6,
            "conv_aux_2": 6,
            "conv_aux_3": 4,
            "conv_aux_4": 4
        }

        # Localization Prediction convolutions
        self.loc_conv_resnet_3 = nn.Conv3d(256, n_boxes['conv_resnet_3'] * 4 * self.n_clip_bboxes, kernel_size=(6, 3, 3), padding=1, stride=(3, 1, 1))
        self.loc_conv_resnet_4 = nn.Conv3d(512, n_boxes['conv_resnet_4'] * 4 * self.n_clip_bboxes, kernel_size=(4, 3, 3), padding=1, stride=(3, 1, 1))
        self.loc_conv_aux_1 = nn.Conv3d(512, n_boxes['conv_aux_1'] * 4 * self.n_clip_bboxes, kernel_size=(2, 3, 3), padding=1, stride=(3, 1, 1))
        self.loc_conv_aux_2 = nn.Conv3d(256, n_boxes['conv_aux_2'] * 4 * self.n_clip_bboxes, kernel_size=(4, 3, 3), padding=1, stride=(3, 1, 1))
        self.loc_conv_aux_3 = nn.Conv3d(256, n_boxes['conv_aux_3'] * 4 * self.n_clip_bboxes, kernel_size=(4, 3, 3), padding=1, stride=(3, 1, 1))
        self.loc_conv_aux_4 = nn.Conv3d(256, n_boxes['conv_aux_4'] * 4 * self.n_clip_bboxes, kernel_size=(4, 3, 3), padding=1, stride=(3, 1, 1))

        # Class prediction convolutions (predict classes in localization boxes)
        self.cl_conv_resnet_3 = nn.Conv3d(256, n_boxes['conv_resnet_3'] * n_classes, kernel_size=3, padding=1)
        self.cl_conv_resnet_4 = nn.Conv3d(512, n_boxes['conv_resnet_4'] * n_classes, kernel_size=3, padding=1)
        self.cl_conv_aux_1 = nn.Conv3d(512, n_boxes['conv_aux_1'] * n_classes, kernel_size=3, padding=1)
        self.cl_conv_aux_2 = nn.Conv3d(256, n_boxes['conv_aux_2'] * n_classes, kernel_size=3, padding=1)
        self.cl_conv_aux_3 = nn.Conv3d(256, n_boxes['conv_aux_3'] * n_classes, kernel_size=3, padding=1)
        self.cl_conv_aux_4 = nn.Conv3d(256, n_boxes['conv_aux_4'] * n_classes, kernel_size=3, padding=1)


    def forward(self, conv_resnet_3_feats, conv_resnet_4_feats, conv_aux_1_feats, conv_aux_2_feats, conv_aux_3_feats, conv_aux_4_feats):
        
        # 
        batch_size = conv_resnet_3_feats.size(0)

        # -------------------------- Localization Prediction ------------------------- #        
        l_conv_res_3 = self.loc_conv_resnet_3(conv_resnet_3_feats)
        l_conv_res_3 = l_conv_res_3.squeeze(2)
        l_conv_res_3 = l_conv_res_3.permute(0, 2, 3, 1).contiguous()
        l_conv_res_3 = l_conv_res_3.view(batch_size, -1, self.n_clip_bboxes, 4)

        l_conv_res_4 = self.loc_conv_resnet_4(conv_resnet_4_feats)
        l_conv_res_4 = l_conv_res_4.squeeze(2)
        l_conv_res_4 = l_conv_res_4.permute(0, 2, 3, 1).contiguous()
        l_conv_res_4 = l_conv_res_4.view(batch_size, -1, self.n_clip_bboxes, 4)

        l_conv_aux_1 = self.loc_conv_aux_1(conv_aux_1_feats)
        l_conv_aux_1 = l_conv_aux_1.squeeze(2)
        l_conv_aux_1 = l_conv_aux_1.permute(0, 2, 3, 1).contiguous()
        l_conv_aux_1 = l_conv_aux_1.view(batch_size, -1, self.n_clip_bboxes, 4)
        
        l_conv_aux_2 = self.loc_conv_aux_2(conv_aux_2_feats)
        l_conv_aux_2 = l_conv_aux_2.squeeze(2)
        l_conv_aux_2 = l_conv_aux_2.permute(0, 2, 3, 1).contiguous()
        l_conv_aux_2 = l_conv_aux_2.view(batch_size, -1, self.n_clip_bboxes, 4)
        
        l_conv_aux_3 = self.loc_conv_aux_3(conv_aux_3_feats)
        l_conv_aux_3 = l_conv_aux_3.squeeze(2)
        l_conv_aux_3 = l_conv_aux_3.permute(0, 2, 3, 1).contiguous()
        l_conv_aux_3 = l_conv_aux_3.view(batch_size, -1, self.n_clip_bboxes, 4)
        
        l_conv_aux_4 = self.loc_conv_aux_4(conv_aux_4_feats)
        l_conv_aux_4 = l_conv_aux_4.squeeze(2)
        l_conv_aux_4 = l_conv_aux_4.permute(0, 2, 3, 1).contiguous()
        l_conv_aux_4 = l_conv_aux_4.view(batch_size, -1, self.n_clip_bboxes, 4)

        prediction_feats = torch.cat([l_conv_res_3, l_conv_res_4, l_conv_aux_1, l_conv_aux_2, l_conv_aux_3, l_conv_aux_4], dim=1)        

        return prediction_feats.permute(0, 2, 1, 3)


class SSDResnet3D(nn.Module):
    """Single Shot Resnet3D network
    Utilizes of a Resnet3D an auxiliary network and predictive region

    Args:
        clip_len: number of frames per clip
        extras: extra layers that feed to multibox loc and conf layers
        head: "multibox head" consists of loc and conf conv layers
    """

    def __init__(self, clip_len, n_classes):
        super(SSDResnet3D, self).__init__()
        
        self.n_classes = n_classes
        self.clip_len = clip_len
        
        # ------------------------------ Generate Priors ----------------------------- #
        self.priors = self.generate_priors(clip_len)

        # ---------------------------- Resnet3D Base Class --------------------------- #
        self.r3d = models.video.r3d_18(True)

        # Layer learns to scale the l2 normalized features from conv4_3
        self.L2Norm = L2Norm(256, 20)

        # -------------------------- Auxiliary Convolutions -------------------------- # 
        self.aux_convs = AuxiliaryConvolutions(n_classes)

        # -------------------------- Prediction Convolutions ------------------------- #
        self.pred_convs = PredictionConvolution(clip_len, n_classes)
        
        phase = 'test'
        if phase == 'test':
            self.softmax = nn.Softmax(dim=-1)

    def forward(self, x):
        feature_maps = {}
        loc = list()
        conf = list()

        # apply resnet3d up to resnet block 3 & 4
        x = self.r3d.stem(x)

        x = self.r3d.layer1(x)
        x = self.r3d.layer2(x)

        x = self.r3d.layer3(x)
        s = self.L2Norm(x) # may have to rescale this layer
        conv_res_3 = s

        x = self.r3d.layer4(x)
        # s = self.L2Norm(x) # may have to rescale this alyer
        conv_res_4 = x

        # Apply auxiliary convolution layers
        conv_aux_1, conv_aux_2, conv_aux_3, conv_aux_4, action_label = self.aux_convs(x)

        # Perform convolution to extract predicted values
        locs = self.pred_convs(conv_res_3, conv_res_4, conv_aux_1, 
                                                conv_aux_2, conv_aux_3, conv_aux_4)

        return locs, action_label

    def generate_priors(self, clip_len):
        """Generates Priors for individual frames
        Structure follows SSD prior
        """
        fmap_dims = {'conv_res_3': 38,
                     'conv_res_4': 19,
                     'conv_aux_1': 10,
                     'conv_aux_2': 5,
                     'conv_aux_3': 3,
                     'conv_aux_4': 1}

        obj_scales = {'conv_res_3': 0.1,
                      'conv_res_4': 0.2,
                      'conv_aux_1': 0.375,
                      'conv_aux_2': 0.55,
                      'conv_aux_3': 0.725,
                      'conv_aux_4': 0.9}

        aspect_ratios = {'conv_res_3': [1., 2., 0.5],
                         'conv_res_4': [1., 2., 3., 0.5, .333],
                         'conv_aux_1': [1., 2., 3., 0.5, .333],
                         'conv_aux_2': [1., 2., 3., 0.5, .333],
                         'conv_aux_3': [1., 2., 0.5],
                         'conv_aux_4': [1., 2., 0.5]}
        
        fmaps = list(fmap_dims.keys())

        prior_boxes = []

        for k, fmap in enumerate(fmaps):
            for h in range(clip_len):
                for i in range(fmap_dims[fmap]):
                    for j in range(fmap_dims[fmap]):
                        cx = (j + 0.5) / fmap_dims[fmap]
                        cy = (i + 0.5) / fmap_dims[fmap]

                        for ratio in aspect_ratios[fmap]:
                            prior_boxes.append([cx, cy, obj_scales[fmap] * sqrt(ratio), obj_scales[fmap] / sqrt(ratio)])

                            # For an aspect ratio of 1, use an additional prior whose scale is the geometric mean of the
                            # scale of the current feature map and the scale of the next feature map
                            if ratio == 1.:
                                try:
                                    additional_scale = sqrt(obj_scales[fmap] * obj_scales[fmaps[k + 1]])
                                # For the last feature map, there is no "next" feature map
                                except IndexError:
                                    additional_scale = 1.
                                prior_boxes.append([cx, cy, additional_scale, additional_scale])

        prior_boxes = torch.FloatTensor(prior_boxes).to(device)  # (8732, 4)
        prior_boxes = prior_boxes.view(50, 8732, 4)
        prior_boxes.clamp_(0, 1)  # (8732, 4)

        return prior_boxes


class CSDResnet3D(SSDResnet3D):
    """Consistency Constraint Resnet3D network
    Utilizes of a Resnet3D an auxiliary network and predictive region

    Args:
        clip_len: number of frames per clip
        extras: extra layers that feed to multibox loc and conf layers
        head: "multibox head" consists of loc and conf conv layers
    """

    def __init__(self, clip_len, n_classes):
        super(CSDResnet3D, self).__init__(clip_len, n_classes)
        

    def forward(self, x, xf):
        """ CSD Forward Routine
            :param x input data of shape [B T H W C]
        """

        # ------------------------ Consistency Constraint Flip ----------------------- #
        # xf = x.clone()
        #xf = torch.flip(xf, (4,))
        #xf = self.horizontal_flip(xf)

        # ----------------------------- CSD Formward Pass ---------------------------- #
        # Regular Forward Pass
        x_locs, x_action_label = super().forward(x)
        # Flipped Forward Pass
        xf_locs, xf_action_label = super().forward(xf)

        # ------------------------------ Return Results ------------------------------ #
        return x_locs, xf_locs, x_action_label, xf_action_label

    def horizontal_flip(self, clip):
        nclip = clip.permute(0, 2, 3, 4, 1).numpy()
        nclip = [[np.fliplr(img) for img in batch] for batch in nclip]
        return torch.tensor(nclip).permute(0, 4, 1, 2, 3)

    def generate_priors(self, clip_len):
        """Generates Priors for individual frames
        Structure follows SSD prior
        """
        fmap_dims = {'conv_res_3': 38,
                     'conv_res_4': 19,
                     'conv_aux_1': 10,
                     'conv_aux_2': 5,
                     'conv_aux_3': 3,
                     'conv_aux_4': 1}

        obj_scales = {'conv_res_3': 0.1,
                      'conv_res_4': 0.2,
                      'conv_aux_1': 0.375,
                      'conv_aux_2': 0.55,
                      'conv_aux_3': 0.725,
                      'conv_aux_4': 0.9}

        aspect_ratios = {'conv_res_3': [1., 2., 0.5],
                         'conv_res_4': [1., 2., 3., 0.5, .333],
                         'conv_aux_1': [1., 2., 3., 0.5, .333],
                         'conv_aux_2': [1., 2., 3., 0.5, .333],
                         'conv_aux_3': [1., 2., 0.5],
                         'conv_aux_4': [1., 2., 0.5]}
        
        fmaps = list(fmap_dims.keys())

        prior_boxes = []

        for k, fmap in enumerate(fmaps):
            for h in range(clip_len):
                for i in range(fmap_dims[fmap]):
                    for j in range(fmap_dims[fmap]):
                        cx = (j + 0.5) / fmap_dims[fmap]
                        cy = (i + 0.5) / fmap_dims[fmap]

                        for ratio in aspect_ratios[fmap]:
                            prior_boxes.append([cx, cy, obj_scales[fmap] * sqrt(ratio), obj_scales[fmap] / sqrt(ratio)])

                            # For an aspect ratio of 1, use an additional prior whose scale is the geometric mean of the
                            # scale of the current feature map and the scale of the next feature map
                            if ratio == 1.:
                                try:
                                    additional_scale = sqrt(obj_scales[fmap] * obj_scales[fmaps[k + 1]])
                                # For the last feature map, there is no "next" feature map
                                except IndexError:
                                    additional_scale = 1.
                                prior_boxes.append([cx, cy, additional_scale, additional_scale])

        prior_boxes = torch.FloatTensor(prior_boxes).to(device)  # (8732, 4)
        prior_boxes = prior_boxes.view(clip_len, 8732, 4)
        prior_boxes.clamp_(0, 1)  # (8732, 4)

        return prior_boxes
      
class ConsistencyLoss(nn.Module):
    def __init__(self):
        super(ConsistencyLoss, self).__init__()
        self.consistency_criterion = torch.nn.KLDivLoss(size_average=False, reduce=False).cuda()
    
    def forward(self, x_loc, xf_loc, x_pred, xf_pred):

        # ---------------------- Consistency Classification Loss --------------------- #
        x_pred = x_pred + 1e-6
        xf_pred = xf_pred + 1e-6

        consistency_confidence_loss_a = self.consistency_criterion(x_pred.log(), xf_pred.detach().log()).sum(-1).mean()
        consistency_confidence_loss_b = self.consistency_criterion(x_pred.log(), xf_pred.detach().log()).sum(-1).mean()
        consistency_confidence_loss = consistency_confidence_loss_a + consistency_confidence_loss_b

        # ----------------------------- Localization Loss ---------------------------- #
        consistency_loc_loss_x = torch.mean(torch.pow(x_loc[:, :, :, 0] + xf_loc[:, :, :, 0], exponent=2))
        consistency_loc_loss_y = torch.mean(torch.pow(x_loc[:, :, :, 1] - xf_loc[:, :, :, 1], exponent=2))
        consistency_loc_loss_w = torch.mean(torch.pow(x_loc[:, :, :, 2] - xf_loc[:, :, :, 2], exponent=2))
        consistency_loc_loss_h = torch.mean(torch.pow(x_loc[:, :, :, 3] - xf_loc[:, :, :, 3], exponent=2))

        consistency_location_loss = torch.div(consistency_loc_loss_x +   \
                                                consistency_loc_loss_y + \
                                                consistency_loc_loss_w + \
                                                consistency_loc_loss_h, 4)

        # -------------------------- Total Consistency Loss -------------------------- #
        consistency_loss = torch.div(consistency_confidence_loss, 2) + consistency_location_loss

        return consistency_loss, consistency_confidence_loss, consistency_location_loss

class MultiBoxLoss(nn.Module):
    """
    The MultiBox loss, a loss function for object detection.

    This is a combination of:
    (1) a localization loss for the predicted locations of the boxes, and
    (2) a confidence loss for the predicted class scores.
    """

    def __init__(self, priors_cxcy, threshold=0.5, neg_pos_ratio=3, alpha=1.):
        super(MultiBoxLoss, self).__init__()
        self.priors_cxcy = priors_cxcy
        self.priors_xy = cxcy_to_xy(priors_cxcy)
        self.threshold = threshold
        self.neg_pos_ratio = neg_pos_ratio
        self.alpha = alpha

        self.smooth_l1 = nn.L1Loss()
        self.cross_entropy = nn.CrossEntropyLoss(reduce=False)

    def forward(self, predicted_locs, predicted_scores, boxes, labels):
        """
        Forward propagation.

        :param predicted_locs: predicted locations/boxes w.r.t the 8732 prior boxes, a tensor of dimensions (N, 8732, 4)
        :param predicted_scores: class scores for each of the encoded locations/boxes, a tensor of dimensions (N, 8732, n_classes)
        :param boxes: true  object bounding boxes in boundary coordinates, a list of N tensors
        :param labels: true object labels, a list of N tensors
        :return: multibox loss, a scalar
        """
        batch_size = predicted_locs.size(0)
        n_frames = predicted_locs.size(1)        
        n_classes = predicted_scores.size(1)
        n_priors = self.priors_cxcy.size(1)

        true_locs = torch.zeros((batch_size, n_frames, 4), dtype=torch.float).to(device)  # (N, n_frames, 8732, 4)
        true_classes = torch.zeros((batch_size, n_frames, n_priors), dtype=torch.long).to(device)  # (N, 8732)

        # For each image
        for i in range(batch_size):
            loc_loss = 0
            n_objects = 1 # Number of objects always 1

            overlap = find_jaccard_overlap(boxes[i],
                                        self.priors_xy[0])  # (n_objects, 8732)


            _, prior_for_each_object = overlap.max(dim=1)  # (N_o)


            # Encode center-size object coordinates into the form we regressed predicted boxes to
            true_locs[i] = cxcy_to_gcxgcy(xy_to_cxcy(boxes[i]), self.priors_cxcy[0][prior_for_each_object])  # (8732, 4)

            # LOCALIZATION LOSS
            # Localization loss is computed only over positive (non-background) priors
            predicted_priors = [predicted_locs[i][frame_idx][prior_for_each_object[frame_idx]] for frame_idx in range(n_frames)]
            predicted_priors = torch.stack(predicted_priors)
            loc_loss += self.smooth_l1(predicted_priors, true_locs[i])  # (), scalar
        
        t_labels = labels[0, :]
        if batch_size > 1:
            t_labels = labels.squeeze()

        # classification loss
        classification_loss = self.cross_entropy(predicted_scores, t_labels)

        # TOTAL LOSS
        return classification_loss.sum(), self.alpha * loc_loss

class CSDCriterion(nn.Module):
    def __init__(self, priors_cxcy):
        super(CSDCriterion, self).__init__()

        self.multibox_loss = MultiBoxLoss(priors_cxcy)
        self.consistency_losss = ConsistencyLoss()

    def forward(self, x_locs, xf_locs, x_preds, xf_preds, boxes, labels):
        multibox_loss = 0
        csd_loss = 0
        if labels.sum() != 0:
            classification_loss, multibox_loss = self.multibox_loss(x_locs, x_preds, boxes, labels)


        csd_loss = self.consistency_losss(x_locs, xf_locs, x_preds, xf_preds)

        final_loss = multibox_loss + csd_loss[0] + classification_loss

        return final_loss, multibox_loss, classification_loss, csd_loss[1], csd_loss[2]


if __name__ == "__main__":
    from data.ucf_101 import UCF101
    from utils import *

    video_transform_list = [video_transforms.Resize((300, 300)),
                volume_transforms.ClipToTensor()]

    transforms = video_transforms.Compose(video_transform_list)

    dataset = UCF101(root="E:\\dev2\\UCF-102",
    annotation_path="./data/annotations",
    frames_per_clip=16, step_between_clips=5, train=False, transform=transforms)
    loader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False,
                                                num_workers=1)
    model = CSDResnet3D(16, n_classes=len(dataset.labels)).to(device)

    frames, lbl, bboxes = next(iter(loader))

    frames_flip = model.horizontal_flip(frames).to(device)
    frames = frames.to(device)
    lbl = lbl.to(device)
    bboxes = bboxes.to(device)
    

    criterion = CSDCriterion(priors_cxcy=model.priors).to(device)

    x_locs, xf_locs, x_action_label, xf_action_label = model.forward(frames, frames_flip)

    loss = criterion(x_locs, xf_locs, x_action_label, xf_action_label, bboxes, lbl)



