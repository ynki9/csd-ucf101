"""
Author: Yannick Roberts

"""

import os
import glob
import torch
from torch.utils.data import Dataset
from PIL import Image
import pickle
import cv2
import numpy as np

from torchvision import transforms, utils

VIDEO_ANNOTATION_FILE = "ucf101_project2.pkl"
VIDEO_EXTENSION = "avi"

class UCF101Dataset(Dataset):
    """UCF101 dataset available from `https://www.crcv.ucf.edu/data/UCF101.php`.

    Parameters
    ----------
    root: string
        Root directory including `train`, and `test` subdirectories.
    split: string
        Indicating which split to return as a data set.
        Valid option: [`train`, `test`]
    transform: torchvision.transforms
        A (series) of valid transformation(s).
    """
    def __init__(self, annotation_path, data_root, split='train', transform=None, target_frame_count=-1, target_frame_skip=1):
        self.annotation_path = os.path.expanduser(annotation_path)
        self.data_root = os.path.expanduser(data_root)
        self.split = split
        self.transform = transform  

        self.channels = 3
        self.time_depth = 3
        self.frame_width = 320
        self.frame_height = 240
        self.target_frame_len = target_frame_count
        self.target_frame_skip = target_frame_skip

        # get location of all avaiable videos
        self.available_videos = sorted(glob.iglob(os.path.join(data_root, '**', '*.%s' % VIDEO_EXTENSION), recursive=True))
        self.available_videos = [self.available_videos[i][2:] for i in range(len(self.available_videos))]
        
        self.annotations = {}
        self.labels = []  # fname - label number mapping
        self.videos = []  # used for in-memory processing
        self.video_id = []

        # build class label - number mapping
        with open(os.path.join(self.annotation_path, VIDEO_ANNOTATION_FILE), 'rb') as fp:
            self.raw_annotation = pickle.load(fp)
            for key, vals in self.raw_annotation.items():
                label = vals['label']
                annotation = vals['annotations']
                frame_count = vals['numf']

                video_path = os.path.join(self.data_root, "{}.{}".format(key, VIDEO_EXTENSION))
                
                # Maybe remove videos with lengths shorter than target frame length

                self.video_id.append(video_path)
                self.annotations[video_path] = annotation
                if label not in self.labels:
                    self.labels.append(label)

        with open('video_paths.txt', 'w') as filehandle:
            for listitem in self.video_id:
                filehandle.write('%s\n' % listitem)


    def __len__(self):
        return len(self.video_id)

    def __getitem__(self, index):
        t_id = self.video_id[index]
        
        video_annotation = self.annotations[t_id]

        _, frames, lbl, bboxes = self.read_video(t_id)
        if self.split == 'test':
            return frames, lbl, bboxes
        else:
            return frames

    def __repr__(self):
        fmt_str = 'Dataset ' + self.__class__.__name__ + '\n'
        fmt_str = 'Dataset path: {}\n'.format(os.path.join(self.annotation_path, VIDEO_ANNOTATION_FILE))
        fmt_str += '    Number of datapoints: {}\n'.format(self.__len__())
        tmp = self.split
        fmt_str += '    Split: {}\n'.format(tmp)
        fmt_str += '    Root Location: {}\n'.format(self.data_root)
        tmp = '    Transforms (if any): '
        fmt_str += '{0}{1}\n'.format(tmp, self.transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
        tmp = '    Target Transforms (if any): '
        fmt_str += '{0}{1}'.format(tmp, self.target_transform.__repr__().replace('\n', '\n' + ' ' * len(tmp)))
        return fmt_str

    def read_video(self, video_idx):
        annotation = self.annotations[video_idx]

        start_frame = annotation[0]['sf']
        end_frame = annotation[0]['ef']
        label = annotation[0]['label']
        box_lbl = annotation[0]['boxes']
        video_path = video_idx
        frame_count = end_frame - start_frame

        # apply timeline ration to decrease frame length
        #start_frame = start_frame + int(((frame_count / 2) - start_frame) * (self.timeline_ration))
        #end_frame = end_frame - int((end_frame - (frame_count / 2)) * (self.timeline_ration))
        #frame_count = end_frame - start_frame
        start_frame = int((frame_count // 2) - (self.target_frame_len // 2))
        end_frame = int((frame_count // 2) + (self.target_frame_len // 2))
        frame_count = int(self.target_frame_len)


        # Open the video file
        frame_channels = 3

        if os.path.exists(video_path) == False:
            print(f"warning: '{video_path}' does not exist.")
            return

        cap = cv2.VideoCapture(video_path)

        
        frames = torch.FloatTensor(
            frame_channels, frame_count, self.frame_resize, self.frame_resize
        )

        bboxes = torch.FloatTensor(
            frame_count, 4
        )

        failed_clip = False
        current_frame = -1
        while cap.isOpened():
            current_frame = current_frame + 1
            ret, frame = cap.read()

            if ret == False:
                if current_frame == 0:
                    print("Video '{}' Skipped!".format(video_path))
                break

            if current_frame >= start_frame and current_frame < end_frame and ret:
                bbox = box_lbl[current_frame - start_frame]
                frame = Image.fromarray(frame)
                frame, bbox = self._transform(frame, bbox)                
                frames[:, current_frame - start_frame, :, :] = frame
                bboxes[current_frame - start_frame] = bbox


        cap.release()
        return failed_clip, frames, label, bboxes

    def _transform(self, image, box):
        # Resize Image
        #resize = transforms.Resize(size=(self.frame_resize, self.frame_resize))
        # image = resize(image)
        w = self.frame_width
        h = self.frame_height

        b_x = box[0]
        b_y = box[1]
        b_w = box[2]
        b_h = box[3]

        c_x = (b_x + b_w) / 2
        c_y  = (b_y + b_h) / 2
        
        c_x /= self.frame_width
        c_y /= self.frame_height
        c_w = b_w / self.frame_width
        c_h = b_h / self.frame_height

        # box = torch.tensor([c_x, c_y, c_w, c_h])
        box = torch.tensor([b_x/w, b_y/h, (b_w+b_x)/w, (b_h+b_y)/h])

        #trans = transforms.ToTensor()
        image = self.transform(image)

        return image, box


if __name__ == '__main__':
    # tiny_train = TinyImageNet('./dataset', split='train')
    # print(len(tiny_train))
    # print(tiny_train.__getitem__(99999))
    # for fname, number in tiny_train.labels.items():
    #     if number == 192:
    #         print(fname, number)

    # tiny_train = TinyImageNet('./dataset', split='val')
    # print(tiny_train.__getitem__(99))

    # in-memory test
    tiny_val = UCF101Dataset("./annotations", "/home/ynk/Downloads/UCF-101", split='test')
    data = tiny_val[0]
    print("Output Shape:")
    print(data.shape)
    pritn("Dataset Parameters")
    print(data)