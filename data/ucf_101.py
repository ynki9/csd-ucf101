import glob
import os
import pickle

from torchvision.datasets.utils import list_dir
from torchvision.datasets.folder import make_dataset
from torchvision.datasets.video_utils import VideoClips

from torchvision.datasets.vision import VisionDataset
import torch


BB_VIDEO_ANNOTATION_FILE = "ucf101_project2.pkl"
BB_VIDEO_LIST_FILE = "bb_trainlist.txt"


class UCF101(VisionDataset):
    """
    UCF101 Dataset Modified from the official Pytorch Implementation to include boundinng boxes

    `UCF101 <https://www.crcv.ucf.edu/data/UCF101.php>`_ dataset.

    UCF101 is an action recognition video dataset.
    This dataset consider every video as a collection of video clips of fixed size, specified
    by ``frames_per_clip``, where the step in frames between each clip is given by
    ``step_between_clips``.

    To give an example, for 2 videos with 10 and 15 frames respectively, if ``frames_per_clip=5``
    and ``step_between_clips=5``, the dataset size will be (2 + 3) = 5, where the first two
    elements will come from video 1, and the next three elements from video 2.
    Note that we drop clips which do not have exactly ``frames_per_clip`` elements, so not all
    frames in a video might be present.

    Internally, it uses a VideoClips object to handle clip creation.

    Args:
        root (string): Root directory of the UCF101 Dataset.
        annotation_path (str): path to the folder containing the split files
        frames_per_clip (int): number of frames in a clip.
        step_between_clips (int, optional): number of frames between each clip.
        fold (int, optional): which fold to use. Should be between 1 and 3.
        train (bool, optional): if ``True``, creates a dataset from the train split,
            otherwise from the ``test`` split.
        transform (callable, optional): A function/transform that  takes in a TxHxWxC video
            and returns a transformed version.

    Returns:
        video (Tensor[T, H, W, C]): the `T` video frames
        audio(Tensor[K, L]): the audio frames, where `K` is the number of channels
            and `L` is the number of points
        label (int): class of the video clip
    """

    def __init__(self, root, annotation_path, frames_per_clip, step_between_clips=1,
                 frame_rate=None, fold=1, train=True, transform=None,
                 _precomputed_metadata=None, num_workers=1, _video_width=0,
                 _video_height=0, _video_min_dimension=0, _audio_samples=0):
        super(UCF101, self).__init__(root)
        if not 1 <= fold <= 3:
            raise ValueError("fold should be between 1 and 3, got {}".format(fold))
        
        # General Initializations
        extensions = ('avi',)
        self.fold = fold
        self.train = train

        classes = list(sorted(list_dir(root)))
        class_to_idx = {classes[i]: i for i in range(len(classes))}
        self.samples = make_dataset(self.root, class_to_idx, extensions, is_valid_file=None)
        self.classes = classes
        self.frames_per_clip = frames_per_clip
        self.step_between_clips = step_between_clips
        self.frame_width = 320
        self.frame_height = 240
        video_list = [x[0] for x in self.samples]
        video_clips = VideoClips(
            video_list,
            frames_per_clip,
            step_between_clips,
            frame_rate,
            _precomputed_metadata,
            num_workers=num_workers,
            _video_width=_video_width,
            _video_height=_video_height,
            _video_min_dimension=_video_min_dimension,
            _audio_samples=_audio_samples,
        )
        self.video_clips_metadata = video_clips.metadata
        self.indices = self._select_fold(video_list, annotation_path, annotation_path, fold, train)
        self.video_clips = video_clips.subset(self.indices)
        self.transform = transform

        
        # build class label - number mapping
        self.annotations = {}
        self.labels = []
        with open(os.path.join(annotation_path, BB_VIDEO_ANNOTATION_FILE), 'rb') as fp:
            self.raw_annotation = pickle.load(fp)
            for key, vals in self.raw_annotation.items():
                label = vals['label']
                annotation = vals['annotations']
                frame_count = vals['numf']

                video_path = os.path.join(root, "{}.{}".format(key, extensions[0]))
                video_path = video_path.replace("/", "\\")
                
                # Maybe remove videos with lengths shorter than target frame length
                self.annotations[video_path] = annotation
                if label not in self.labels:
                    self.labels.append(label)

    @property
    def metadata(self):
        return self.video_clips_metadata

    def _select_fold(self, video_list, bb_annotation_path, annotation_path, fold, train):
        name = "train" if train else "test"
        selected_files = []
        if train: # For validation only use bb set
            name = "{}list{:02d}.txt".format(name, fold)
            f = os.path.join(annotation_path, name)
            with open(f, "r") as fid:
                data = fid.readlines()
                data = [x.strip().split(" ") for x in data]
                data = [x[0] for x in data]
                selected_files.extend(data)

        with open(os.path.join(bb_annotation_path, BB_VIDEO_LIST_FILE), "r") as fid:
            data = fid.readlines()
            data = [x.strip().split(" ") for x in data]
            data = [x[0] for x in data]
            selected_files.extend(data)

        selected_files = set(selected_files)
        indices = [i for i in range(len(video_list)) if video_list[i][len(self.root) + 1:] in selected_files]
        return indices

    def __len__(self):
        return self.video_clips.num_clips()

    def __getitem__(self, idx):
        # Gets video clip
        video, _, info, video_idx = self.video_clips.get_clip(idx)
        t_path = self.video_clips.video_paths[video_idx]
        # for windows fix path for indexing
        t_path = t_path.replace("/", "\\")
        bboxes = torch.FloatTensor(
            self.frames_per_clip, 4
        )
        bboxes[:, :] = 0

        label = torch.tensor([0], dtype=torch.long)
        
        if t_path in self.annotations.keys():
            anno = self.annotations[t_path]
            _, clip_index = self.video_clips.get_clip_location(idx)
            bboxes, label = self._get_bounding_region(anno, clip_index)            


        if self.transform is not None:
            video = self.transform(video.numpy())
            video = video.permute(0, 1, 2, 3)

        return video, label, bboxes

    def _get_bounding_region(self, annotation, clip_pos):
        # check if bb is avaialbe

        start_frame = annotation[0]['sf']
        end_frame = annotation[0]['ef']
        label = annotation[0]['label']
        box_lbl = annotation[0]['boxes']
        frame_count = end_frame - start_frame

        
        bboxes = torch.FloatTensor(
            self.frames_per_clip, 4
        )
        
        blabel = torch.tensor([label], dtype=torch.long)        

        for i in range(self.frames_per_clip):
            x = clip_pos + i * self.step_between_clips
            if x in range(len(box_lbl)):
                t_box = box_lbl[x]
                t_box = self._transform_box(t_box)
                bboxes[i] = t_box
            else:
                return None
        
        return bboxes, blabel

      
    def _transform_box(self, box):
        w = float(self.frame_width)
        h = float(self.frame_height)

        b_x = float(box[0])
        b_y = float(box[1])
        b_w = float(box[2])
        b_h = float(box[3])

        c_x = (b_x + b_w) / 2
        c_y  = (b_y + b_h) / 2
        
        c_x /= self.frame_width
        c_y /= self.frame_height
        c_w = b_w / self.frame_width
        c_h = b_h / self.frame_height

        # box = torch.tensor([c_x, c_y, c_w, c_h])
        box = torch.FloatTensor([b_x/w, b_y/h, (b_w+b_x)/w, (b_h+b_y)/h])

        return box



if __name__ == "__main__":
    dataset = UCF101(root="E:\\dev2\\UCF-102",
    annotation_path="./data/annotations",
    frames_per_clip=16, step_between_clips=5, train=True)
    loader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=False,
                                                num_workers=1)

    frames, lbl, bboxes = next(iter(loader))


    print("test")
    